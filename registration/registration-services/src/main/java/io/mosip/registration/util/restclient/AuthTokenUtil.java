package io.mosip.registration.util.restclient;

/**
 * The utility class to get the Authentication Token from Kernel's
 * Authentication Web-Service.<br> <br>
 * 
 * The authentication will be based on any one of the following three types:<br>1.
 * User Id and Password<br>2. User Id and OTP<br>3. Client Id and Secret Key
 * 
 * @author Balaji Sridharn
 * @author Mahesh Kumar
 * @since 1.0.0
 */
public class AuthTokenUtil {

	public AuthTokenUtil() {
		
	}
}
