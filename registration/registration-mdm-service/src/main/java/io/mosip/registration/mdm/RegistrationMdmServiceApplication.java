package io.mosip.registration.mdm;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RegistrationMdmServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(RegistrationMdmServiceApplication.class, args);
	}

}
