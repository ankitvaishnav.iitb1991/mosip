\c mosip_ida sysadmin

\ir ddl/ida-auth_transaction.sql
\ir ddl/ida-static_pin.sql
\ir ddl/ida-static_pin_h.sql
\ir ddl/ida-vid.sql
\ir ddl/ida-token_seed.sql
\ir ddl/ida-token_seq.sql
\ir ddl/ida-vid_seed.sql
\ir ddl/ida-vid_seq.sql

\ir ddl/AlterFK_ida_schema.sql

