package io.mosip.admin.uinmgmt.dto;


import lombok.Data;

/**
 * Response DTO For UIN Status
 * 
 * @author Megha Tanga
 */
@Data
public class UinResponseDto {
	/**
	 * Field for UIn Status
	 */
	String status;
	
	
}
