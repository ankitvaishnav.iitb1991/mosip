package io.mosip.admin.uinmgmt.dto;

import io.mosip.kernel.core.http.ResponseWrapper;

/**
 * Response DTO 
 * 
 * @author Megha Tanga
 * 
 */
public class UinResponseWrapperDto extends ResponseWrapper<UinResponseDto>{

}
