package io.mosip.admin.navigation.constant;

public abstract class LoginUri {

    public static final String VALIDATE_USER = "/useridPwd";
    public static final String INVALIDATE_TOKEN = "/invalidateToken";

}
