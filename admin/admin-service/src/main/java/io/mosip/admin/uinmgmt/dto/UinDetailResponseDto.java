package io.mosip.admin.uinmgmt.dto;

import java.util.Map;

import lombok.Data;

@Data
public class UinDetailResponseDto {
	
	public  Map<String, String> uinProperties;

}
