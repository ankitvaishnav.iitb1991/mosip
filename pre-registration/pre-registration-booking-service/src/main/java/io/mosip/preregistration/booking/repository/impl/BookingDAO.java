/* 
 * Copyright
 * 
 */
package io.mosip.preregistration.booking.repository.impl;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import io.mosip.kernel.core.dataaccess.exception.DataAccessLayerException;
import io.mosip.preregistration.booking.entity.AvailibityEntity;
import io.mosip.preregistration.booking.entity.RegistrationBookingEntity;
import io.mosip.preregistration.booking.errorcodes.ErrorCodes;
import io.mosip.preregistration.booking.errorcodes.ErrorMessages;
import io.mosip.preregistration.booking.exception.AppointmentBookingFailedException;
import io.mosip.preregistration.booking.exception.AvailabilityUpdationFailedException;
import io.mosip.preregistration.booking.exception.AvailablityNotFoundException;
import io.mosip.preregistration.booking.exception.BookingDataNotFoundException;
import io.mosip.preregistration.booking.exception.CancelAppointmentFailedException;
import io.mosip.preregistration.booking.exception.RecordFailedToDeleteException;
import io.mosip.preregistration.booking.exception.RecordNotFoundException;
import io.mosip.preregistration.booking.repository.BookingAvailabilityRepository;
import io.mosip.preregistration.booking.repository.DemographicRepository;
import io.mosip.preregistration.booking.repository.RegistrationBookingRepository;
import io.mosip.preregistration.core.common.entity.DemographicEntity;
import io.mosip.preregistration.core.exception.InvalidRequestParameterException;
import io.mosip.preregistration.core.exception.TableNotAccessibleException;

/**
 * This repository class is used to implement the JPA methods for Booking
 * application.
 * 
 * @author Kishan Rathore
 * @since 1.0.0
 *
 */
@Component
public class BookingDAO {

	/** Autowired reference for {@link #bookingRepository}. */
	@Autowired
	@Qualifier("bookingAvailabilityRepository")
	private BookingAvailabilityRepository bookingAvailabilityRepository;

	/** Autowired reference for {@link #registrationBookingRepository}. */
	@Autowired
	@Qualifier("registrationBookingRepository")
	private RegistrationBookingRepository registrationBookingRepository;
	
	@Autowired
	@Qualifier("demographicRepository")
	private DemographicRepository demographicRepository;

	/**
	 * @param Registration
	 *            center id
	 * @param Registration
	 *            date
	 * @return List AvailibityEntity based registration id and registration date.
	 */
	public List<AvailibityEntity> availability(String regcntrId, LocalDate regDate) {
		List<AvailibityEntity> availabilityList = null;
		try {
			availabilityList = bookingAvailabilityRepository.findByRegcntrIdAndRegDateOrderByFromTimeAsc(regcntrId,
					regDate);
		} catch (DataAccessLayerException e) {
			throw new AvailablityNotFoundException(ErrorCodes.PRG_BOOK_RCI_016.getCode(),
					ErrorMessages.AVAILABILITY_TABLE_NOT_ACCESSABLE.getMessage());
		}
		return availabilityList;

	}

	/**
	 * @param entity
	 * @return boolean
	 */
	public boolean saveBookAppointment(RegistrationBookingEntity entity) {
		return registrationBookingRepository.save(entity) != null;
	}

	/**
	 * @param regcntrId
	 * @param fromDate
	 * @param toDate
	 * @return List of Local date
	 */
	public List<LocalDate> findDate(String regcntrId, LocalDate fromDate, LocalDate toDate) {
		List<LocalDate> localDatList = null;
		try {
			localDatList = bookingAvailabilityRepository.findDate(regcntrId, fromDate, toDate);
			if (localDatList == null || localDatList.isEmpty()) {
				throw new RecordNotFoundException(ErrorCodes.PRG_BOOK_RCI_015.getCode(),
						ErrorMessages.NO_TIME_SLOTS_ASSIGNED_TO_THAT_REG_CENTER.getMessage());
			}
		} catch (DataAccessLayerException e) {
			throw new TableNotAccessibleException(ErrorCodes.PRG_BOOK_RCI_016.getCode(),
					ErrorMessages.AVAILABILITY_TABLE_NOT_ACCESSABLE.getMessage());
		}
		return localDatList;
	}

	/**
	 * @param slotFromTime
	 * @param slotToTime
	 * @param regDate
	 * @param regcntrd
	 * @return Availibity Entity based on FromTime, ToTime, RegDate and RegcntrId.
	 */
	public AvailibityEntity findByFromTimeAndToTimeAndRegDateAndRegcntrId(LocalTime slotFromTime, LocalTime slotToTime,
			LocalDate regDate, String regcntrd) {
		AvailibityEntity entity = null;
		try {
			entity = bookingAvailabilityRepository.findByFromTimeAndToTimeAndRegDateAndRegcntrId(slotFromTime,
					slotToTime, regDate, regcntrd);
			if (entity == null) {

				throw new AvailablityNotFoundException(ErrorCodes.PRG_BOOK_RCI_002.getCode(),
						ErrorMessages.AVAILABILITY_NOT_FOUND_FOR_THE_SELECTED_TIME.getMessage());
			}

		} catch (DataAccessLayerException e) {
			throw new TableNotAccessibleException(ErrorCodes.PRG_BOOK_RCI_016.getCode(),
					ErrorMessages.AVAILABILITY_TABLE_NOT_ACCESSABLE.getMessage());
		}
		return entity;
	}

	/**
	 * This method find entity for status other then CANCEL.
	 * 
	 * @param preregistrationId
	 * @param statusCode
	 * @return RegistrationBookingEntity based on Pre registration id and status
	 *         code.
	 */
	public RegistrationBookingEntity findByPreRegistrationId(String preregistrationId) {
		RegistrationBookingEntity entity = null;
		try {
			entity = registrationBookingRepository.getPreRegId(preregistrationId);
			if (entity == null) {
				throw new BookingDataNotFoundException(ErrorCodes.PRG_BOOK_RCI_013.getCode(),
						ErrorMessages.BOOKING_DATA_NOT_FOUND.getMessage());
			}
		} catch (DataAccessLayerException e) {
			throw new TableNotAccessibleException(ErrorCodes.PRG_BOOK_RCI_016.getCode(),
					ErrorMessages.BOOKING_TABLE_NOT_ACCESSIBLE.getMessage());
		}
		return entity;
	}

	/**
	 * @param bookingEnity
	 * @return RegistrationBookingEntity
	 */
	public RegistrationBookingEntity saveRegistrationEntityForCancel(RegistrationBookingEntity bookingEnity) {
		RegistrationBookingEntity entity = null;
		try {
			entity = registrationBookingRepository.save(bookingEnity);
			if (entity == null) {
				throw new CancelAppointmentFailedException(ErrorCodes.PRG_BOOK_RCI_019.getCode(),
						ErrorMessages.APPOINTMENT_CANCEL_FAILED.getMessage());
			}
		} catch (DataAccessLayerException e) {
			throw new TableNotAccessibleException(ErrorCodes.PRG_BOOK_RCI_016.getCode(),
					ErrorMessages.BOOKING_TABLE_NOT_ACCESSIBLE.getMessage());
		}
		return entity;
	}

	/**
	 * @param availibityEntity
	 * @return AvailibityEntity
	 */
	public AvailibityEntity updateAvailibityEntity(AvailibityEntity availibityEntity) {
		AvailibityEntity entity = null;
		try {
			entity = bookingAvailabilityRepository.update(availibityEntity);
			if (entity == null) {
				throw new AvailabilityUpdationFailedException(ErrorCodes.PRG_BOOK_RCI_024.getCode(),
						ErrorMessages.AVAILABILITY_UPDATE_FAILED.getMessage());
			}
		} catch (DataAccessLayerException e) {
			throw new TableNotAccessibleException(ErrorCodes.PRG_BOOK_RCI_016.getCode(),
					ErrorMessages.AVAILABILITY_TABLE_NOT_ACCESSABLE.getMessage());
		}
		return entity;
	}

	/**
	 * @param bookingEntity
	 * @return RegistrationBookingEntity
	 */
	public RegistrationBookingEntity saveRegistrationEntityForBooking(RegistrationBookingEntity bookingEntity) {
		RegistrationBookingEntity entity = null;
		try {
			entity = registrationBookingRepository.save(bookingEntity);
			if (entity == null) {
				throw new AppointmentBookingFailedException(ErrorCodes.PRG_BOOK_RCI_005.getCode(),
						ErrorMessages.APPOINTMENT_BOOKING_FAILED.getMessage());
			}
		} catch (DataAccessLayerException e) {
			throw new TableNotAccessibleException(ErrorCodes.PRG_BOOK_RCI_016.getCode(),
					ErrorMessages.BOOKING_TABLE_NOT_ACCESSIBLE.getMessage());
		}
		return entity;
	}

	/**
	 * @param registrationCenterId
	 * @param statusCode
	 * @return List of RegistrationBookingEntity
	 */
	public List<RegistrationBookingEntity> findByRegistrationCenterId(String registrationCenterId) {
		List<RegistrationBookingEntity> entityList;
		try {
			entityList = registrationBookingRepository.findByRegistrationCenterId(registrationCenterId);
			if (entityList.isEmpty()) {
				throw new BookingDataNotFoundException(ErrorCodes.PRG_BOOK_RCI_013.getCode(),
						ErrorMessages.BOOKING_DATA_NOT_FOUND.getMessage());
			}
		} catch (DataAccessLayerException e) {
			throw new TableNotAccessibleException(ErrorCodes.PRG_BOOK_RCI_016.getCode(),
					ErrorMessages.BOOKING_TABLE_NOT_ACCESSIBLE.getMessage());
		}
		return entityList;
	}

	/**
	 * @param regcntrId
	 * @param regDate
	 * @return List of AvailibityEntity
	 */
	public List<AvailibityEntity> findByRegcntrIdAndRegDateOrderByFromTimeAsc(String regcntrId, LocalDate regDate) {

		List<AvailibityEntity> entityList = null;
		try {
			entityList = bookingAvailabilityRepository.findByRegcntrIdAndRegDateOrderByFromTimeAsc(regcntrId, regDate);
		} catch (DataAccessLayerException e) {
			throw new TableNotAccessibleException(ErrorCodes.PRG_BOOK_RCI_016.getCode(),
					ErrorMessages.AVAILABILITY_TABLE_NOT_ACCESSABLE.getMessage());
		}
		return entityList;
	}

	/**
	 * @param entity
	 * @return boolean
	 */
	public boolean saveAvailability(AvailibityEntity entity) {
		return bookingAvailabilityRepository.save(entity) != null;
	}

	public List<RegistrationBookingEntity> findByPreregistrationId(String preId) {
		List<RegistrationBookingEntity> entityList = null;
		try {
			entityList = registrationBookingRepository.findBypreregistrationId(preId);
			if (entityList.isEmpty()) {
				throw new BookingDataNotFoundException(ErrorCodes.PRG_BOOK_RCI_013.getCode(),
						ErrorMessages.BOOKING_DATA_NOT_FOUND.getMessage());
			}
		} catch (DataAccessLayerException e) {
			throw new TableNotAccessibleException(ErrorCodes.PRG_BOOK_RCI_016.getCode(),
					ErrorMessages.BOOKING_TABLE_NOT_ACCESSIBLE.getMessage());
		}
		return entityList;

	}

	public int deleteByPreRegistrationId(String preId) {
		int count = registrationBookingRepository.deleteByPreRegistrationId(preId);
		if (count == 0) {
			throw new RecordFailedToDeleteException(ErrorCodes.PRG_BOOK_RCI_028.getCode(),
					ErrorMessages.FAILED_TO_DELETE_THE_PRE_REGISTRATION_RECORD.getMessage());
		}
		return count;
	}

	public void deleteRegistrationEntity(RegistrationBookingEntity bookingEnity) {
		try {
			registrationBookingRepository.delete(bookingEnity);
		} catch (DataAccessLayerException e) {
			throw new TableNotAccessibleException(ErrorCodes.PRG_BOOK_RCI_016.getCode(),
					ErrorMessages.BOOKING_TABLE_NOT_ACCESSIBLE.getMessage());
		}
	}

	/**
	 * @param fromLocaldate
	 * @param toLocaldate
	 * @return
	 */
	public List<String> findByBookingDateBetweenAndRegCenterId(LocalDate fromLocaldate, LocalDate toLocaldate,
			String regCenterId) {
		List<String> listOfPreIds = new ArrayList<>();
		try {
			if (regCenterId != null && !regCenterId.isEmpty()) {
				List<RegistrationBookingEntity> entities = registrationBookingRepository
						.findByRegDateBetweenAndRegistrationCenterId(fromLocaldate, toLocaldate, regCenterId);
				if (entities != null && !entities.isEmpty()) {
					for (RegistrationBookingEntity entity : entities) {
						listOfPreIds.add(entity.getBookingPK().getPreregistrationId());
					}
				} else {
					throw new BookingDataNotFoundException(ErrorCodes.PRG_BOOK_RCI_032.getCode(),
							ErrorMessages.RECORD_NOT_FOUND_FOR_DATE_RANGE_AND_REG_CENTER_ID.getMessage());
				}
			} else {
				throw new InvalidRequestParameterException(ErrorCodes.PRG_BOOK_RCI_007.getCode(),
						ErrorMessages.REGISTRATION_CENTER_ID_NOT_ENTERED.getMessage(), null);
			}
		} catch (DataAccessLayerException e) {
			throw new BookingDataNotFoundException(ErrorCodes.PRG_BOOK_RCI_032.getCode(),
					ErrorMessages.RECORD_NOT_FOUND_FOR_DATE_RANGE_AND_REG_CENTER_ID.getMessage());
		}
		return listOfPreIds;
	}
	
	/**
	 * 
	 * @param regDate
	 * @return list of date
	 */
	public List<LocalDate> findDateDistinct(LocalDate regDate) {
		List<LocalDate> localDatList = null;
		try {
			localDatList = bookingAvailabilityRepository.findAvaialableDate(regDate);
		} catch (DataAccessLayerException e) {
			throw new TableNotAccessibleException(ErrorCodes.PRG_BOOK_RCI_016.getCode(),
					ErrorMessages.AVAILABILITY_TABLE_NOT_ACCESSABLE.getMessage());
		}
		return localDatList;
	}
	
	/**
	 * 
	 * This method will update the booking status in applicant table.
	 * 
	 * @param preRegId
	 * @param status
	 * @return
	 */
	public DemographicEntity updateDemographicStatus(String preRegId, String status) {
		DemographicEntity demographicEntity = null;

		try {
			demographicEntity = demographicRepository.findBypreRegistrationId(preRegId);

			if (demographicEntity == null) {
				throw new RecordNotFoundException(ErrorCodes.PRG_PAM_APP_005.getCode(),
						ErrorMessages.UNABLE_TO_FETCH_THE_PRE_REGISTRATION.getMessage());
			}
		} catch (DataAccessLayerException e) {
			throw new BookingDataNotFoundException(ErrorCodes.PRG_BOOK_RCI_032.getCode(),
					ErrorMessages.RECORD_NOT_FOUND_FOR_DATE_RANGE_AND_REG_CENTER_ID.getMessage());
		}
		
		demographicEntity.setStatusCode(status);
		demographicRepository.save(demographicEntity);
		return demographicEntity;


	}

}
