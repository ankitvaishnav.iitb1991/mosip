package io.mosip.kernel.syncjob;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SyncjobBootApplication 
{
    
    /**
     * The main method to start the project.
     *
     * @param args the arguments
     */
    public static void main( String[] args )
    {
        SpringApplication.run(SyncjobBootApplication.class, args);
    }
}
