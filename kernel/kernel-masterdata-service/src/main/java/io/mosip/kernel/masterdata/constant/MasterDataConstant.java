package io.mosip.kernel.masterdata.constant;

/**
 * @author Dharmesh Khandelwal
 * @author Urvil Joshi
 *
 * @since 1.0.0
 */
public class MasterDataConstant {
	/**
	 * Constructor for this class
	 */
	private MasterDataConstant() {
	}

	public static final Double METERTOMILECONVERSION = 0.000621371;
	public static final String DATETIMEFORMAT = " format should be yyyy-mm-ddThh:mm:ss format";
	public static final String VALID = "Valid";
	public static final String INVALID = "Invalid";
}
