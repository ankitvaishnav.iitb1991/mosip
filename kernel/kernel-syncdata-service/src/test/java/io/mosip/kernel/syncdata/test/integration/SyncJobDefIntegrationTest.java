package io.mosip.kernel.syncdata.test.integration;

import static org.springframework.test.web.client.match.MockRestRequestMatchers.requestTo;
import static org.springframework.test.web.client.response.MockRestResponseCreators.withServerError;
import static org.springframework.test.web.client.response.MockRestResponseCreators.withStatus;
import static org.springframework.test.web.client.response.MockRestResponseCreators.withSuccess;
import static org.springframework.test.web.client.response.MockRestResponseCreators.withUnauthorizedRequest;

import java.time.LocalDateTime;
import java.time.ZoneOffset;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.test.context.support.WithUserDetails;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.client.MockRestServiceServer;
import org.springframework.web.client.RestTemplate;

import io.mosip.kernel.auth.adapter.exception.AuthNException;
import io.mosip.kernel.auth.adapter.exception.AuthZException;
import io.mosip.kernel.syncdata.exception.ParseResponseException;
import io.mosip.kernel.syncdata.exception.SyncDataServiceException;
import io.mosip.kernel.syncdata.exception.SyncServiceException;
import io.mosip.kernel.syncdata.service.SyncJobDefService;
import io.mosip.kernel.syncdata.test.TestBootApplication;

@SpringBootTest(classes = TestBootApplication.class)
@RunWith(SpringRunner.class)
@AutoConfigureMockMvc
public class SyncJobDefIntegrationTest {

	@Autowired
	private RestTemplate restTemplate;

	@Autowired
	SyncJobDefService syncJobDefService;

	private static final String JSON_SYNC_JOB_DEF = "{ \"id\": null, \"version\": null, \"responsetime\": \"2019-04-02T07:49:18.454Z\", \"metadata\": null, \"response\": { \"syncJobDefinitions\": [ { \"id\": \"LCS_J00002\", \"name\": \"Login Credentials Sync\", \"apiName\": null, \"parentSyncJobId\": \"NULL\", \"syncFreq\": \"0 0 11 * * ?\", \"lockDuration\": \"NULL\"  ] }, \"errors\": null } ";
	private String TOKEN_EXPIRED_ERROR_MESSAGE = "{ \"id\": null, \"version\": null, \"responsetime\": \"2019-05-11T11:02:20.521Z\", \"metadata\": null, \"response\": null, \"errors\": [ { \"errorCode\": \"KER-ATH-402\", \"message\": \"Token expired\" } ] }";
	private String FORBIDDEN_ERROR_MESSAGE = "{ \"id\": null, \"version\": null, \"responsetime\": \"2019-05-11T11:02:20.521Z\", \"metadata\": null, \"response\": null, \"errors\": [ { \"errorCode\": \"KER-ATH-403\", \"message\": \"Forbidden\" } ] }";

	@Value("${mosip.kernel.syncdata.syncjob-base-url}")
	private String baseUri;
	
	private LocalDateTime lastUpdatedTime=LocalDateTime.now(ZoneOffset.UTC);
	
	private LocalDateTime currentTimeStamp=LocalDateTime.now(ZoneOffset.UTC).minusHours(1);

	@Test(expected=BadCredentialsException.class)
	@WithUserDetails(value = "reg-officer")
	public void syncJobDefUnAuthZException() throws Exception {

		MockRestServiceServer server = MockRestServiceServer.bindTo(restTemplate).build();
		server.expect(requestTo(baseUri + "?lastupdatedtimestamp="+lastUpdatedTime+"Z")).andRespond(withUnauthorizedRequest());
		syncJobDefService.getSyncJobDefDetails(lastUpdatedTime, currentTimeStamp);
	}

	@Test(expected=AccessDeniedException.class)
	@WithUserDetails(value = "reg-officer")
	public void syncJobDefForbiddenException() throws Exception {

		MockRestServiceServer server = MockRestServiceServer.bindTo(restTemplate).build();
		server.expect(requestTo(baseUri + "?lastupdatedtimestamp="+lastUpdatedTime+"Z")).andRespond(withStatus(HttpStatus.FORBIDDEN));
		syncJobDefService.getSyncJobDefDetails(lastUpdatedTime, currentTimeStamp);

	}

	@Test(expected=AuthNException.class)
	@WithUserDetails(value = "reg-officer")
	public void syncJobDefTokenExpiredException() throws Exception {

		MockRestServiceServer server = MockRestServiceServer.bindTo(restTemplate).build();
		server.expect(requestTo(baseUri + "?lastupdatedtimestamp="+lastUpdatedTime+"Z"))
				.andRespond(withUnauthorizedRequest().body(TOKEN_EXPIRED_ERROR_MESSAGE));
		syncJobDefService.getSyncJobDefDetails(lastUpdatedTime, currentTimeStamp);

	}

	@Test(expected=AuthZException.class)
	@WithUserDetails(value = "reg-officer")
	public void syncJobForbiddenException() throws Exception {

		MockRestServiceServer server = MockRestServiceServer.bindTo(restTemplate).build();
		server.expect(requestTo(baseUri + "?lastupdatedtimestamp="+lastUpdatedTime+"Z"))
				.andRespond(withStatus(HttpStatus.FORBIDDEN).body(FORBIDDEN_ERROR_MESSAGE));
		syncJobDefService.getSyncJobDefDetails(lastUpdatedTime, currentTimeStamp);

	}
	
	@Test(expected=SyncDataServiceException.class)
	@WithUserDetails(value = "reg-officer")
	public void syncJobSyncServiceException() throws Exception {

		MockRestServiceServer server = MockRestServiceServer.bindTo(restTemplate).build();
		server.expect(requestTo(baseUri + "?lastupdatedtimestamp="+lastUpdatedTime+"Z"))
				.andRespond(withServerError());
		syncJobDefService.getSyncJobDefDetails(lastUpdatedTime, currentTimeStamp);

	}
	
	@Test(expected=SyncServiceException.class)
	@WithUserDetails(value = "reg-officer")
	public void syncJobServiceException() throws Exception {

		MockRestServiceServer server = MockRestServiceServer.bindTo(restTemplate).build();
		server.expect(requestTo(baseUri + "?lastupdatedtimestamp="+lastUpdatedTime+"Z"))
				.andRespond(withSuccess().body(TOKEN_EXPIRED_ERROR_MESSAGE));
		syncJobDefService.getSyncJobDefDetails(lastUpdatedTime, currentTimeStamp);

	}
	
	@Test(expected=ParseResponseException.class)
	@WithUserDetails(value = "reg-officer")
	public void syncJobIOException() throws Exception {

		MockRestServiceServer server = MockRestServiceServer.bindTo(restTemplate).build();
		server.expect(requestTo(baseUri + "?lastupdatedtimestamp="+lastUpdatedTime+"Z"))
				.andRespond(withSuccess().body(JSON_SYNC_JOB_DEF));
		syncJobDefService.getSyncJobDefDetails(lastUpdatedTime, currentTimeStamp);

	}
}
