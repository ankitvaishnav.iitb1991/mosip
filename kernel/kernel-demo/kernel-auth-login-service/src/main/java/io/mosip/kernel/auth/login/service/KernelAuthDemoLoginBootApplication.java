/*
 * 
 * 
 * 
 * 
 */
package io.mosip.kernel.auth.login.service;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


@SpringBootApplication
public class KernelAuthDemoLoginBootApplication {

	/**
	 * Main method for this application
	 * 
	 * @param args arguments to pass
	 */
	public static void main(String[] args) {
		SpringApplication.run(KernelAuthDemoLoginBootApplication.class, args);
	}
}
