package io.mosip.authentication.core.spi.provider.bio;

import io.mosip.authentication.core.spi.bioauth.provider.MosipBiometricProvider;

/**
 * The Interface MosipFingerprintProvider.
 *
 * @author Dinesh Karuppiah.T
 */
public interface MosipFaceProvider extends MosipBiometricProvider {

}