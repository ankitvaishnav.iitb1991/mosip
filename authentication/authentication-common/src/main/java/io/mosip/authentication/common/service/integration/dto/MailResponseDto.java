package io.mosip.authentication.common.service.integration.dto;

import lombok.Data;

/**
 * 
 * @author Dinesh Karuppiah
 */
@Data
public class MailResponseDto {

	/**
	 * Variable to hold status value
	 */
	String status;
}
