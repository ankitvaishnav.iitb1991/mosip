package io.mosip.authentication.common.service.integration.dto;

import lombok.Data;

@Data
public class OtpGeneratorResponseDto {

	String status;
	String message;

}
