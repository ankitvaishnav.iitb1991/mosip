package io.mosip.authentication.common.service.policy.dto;

import lombok.Data;

@Data
public class Policies {
	private Policy policies;
}
