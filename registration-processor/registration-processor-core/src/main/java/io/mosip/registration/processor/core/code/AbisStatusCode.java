package io.mosip.registration.processor.core.code;
/**
 * 
 * @author Girish Yarru
 *
 */
public enum AbisStatusCode {
	IN_PROGRESS,
	SENT, 
	PROCESSED, 
	FAILED,
	SUCCESS,
	
	INSERT,
	IDENTIFY

}
